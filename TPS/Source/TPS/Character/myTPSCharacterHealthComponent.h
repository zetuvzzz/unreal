// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/TPSHealthComponent.h"
#include "myTPSCharacterHealthComponent.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UmyTPSCharacterHealthComponent : public UTPSHealthComponent
{
	GENERATED_BODY()
	
};
